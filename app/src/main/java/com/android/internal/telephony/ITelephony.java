
/**
 * Created by minhhung on 1/23/2018.
 */

package com.android.internal.telephony;


public interface ITelephony {

    boolean endCall();

    void answerRingingCall();

    void silenceRinger();
}
