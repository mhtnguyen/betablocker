package teltech.betablocker.calllog;

/**
 * Created by minhhung on 1/25/2018.
 */

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CallLog;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import teltech.betablocker.model.CallEntry;

public class CallLogHelper {

    public static ArrayList<CallEntry> getAllCallLogs(Context context, ContentResolver cr) {
        ArrayList<CallEntry> rejected = new ArrayList<CallEntry>();
        final String[] projection = new String[] {
                CallLog.Calls._ID,
                CallLog.Calls.NUMBER,
                CallLog.Calls.DATE,
                CallLog.Calls.DURATION,
                CallLog.Calls.TYPE
        };
        final String selection = null;
        final String[] selectionArgs = null;
        final String sortOrder = android.provider.CallLog.Calls.DATE + " DESC";
        Cursor cursor = null;
        try{
            cursor = context.getContentResolver().query(
                    Uri.parse("content://call_log/calls"),
                    projection,
                    selection,
                    selectionArgs,
                    sortOrder);
            while (cursor.moveToNext()) {
                String callLogID =           cursor.getString(cursor.getColumnIndex(android.provider.CallLog.Calls._ID));
                String callNumber = cursor.getString(cursor.getColumnIndex(android.provider.CallLog.Calls.NUMBER));
                String callDate = cursor.getString(cursor.getColumnIndex(android.provider.CallLog.Calls.DATE));
                String callType = cursor.getString(cursor.getColumnIndex(android.provider.CallLog.Calls.TYPE));
                //String isCallNew = cursor.getString(cursor.getColumnIndex(android.provider.CallLog.Calls.NEW));
                if(Integer.parseInt(callType) == CallLog.Calls.REJECTED_TYPE ){
                    Log.v(CallLogHelper.class.getName(),"rejected Call Found: " + callNumber);
                    SimpleDateFormat formatter = new SimpleDateFormat(
                            "dd-MMM-yyyy HH:mm");
                    String dateString = formatter.format(new Date(Long
                            .parseLong(callDate)));
                    CallEntry callEntry = new CallEntry(callNumber,dateString);
                    rejected.add(callEntry);
                }
            }
        } catch(Exception ex){
            Log.e(CallLogHelper.class.getName(),"ERROR: " + ex.toString());
        }finally{
            cursor.close();
        }
        return rejected;
    }


}
