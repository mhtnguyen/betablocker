package teltech.betablocker.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import teltech.betablocker.R;
import teltech.betablocker.model.BlackList;
import teltech.betablocker.model.CallEntry;

/**
 * Created by minhhung on 1/23/2018.
 */

public class HistoryRecyclerViewAdapter extends RecyclerView.Adapter<History_View_Holder> {

    List<CallEntry> list = Collections.emptyList();
    Context context;

    public HistoryRecyclerViewAdapter(List<CallEntry> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public History_View_Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        //Inflate the layout, initialize the View Holder
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.history_item, parent, false);
        History_View_Holder holder = new History_View_Holder(v);
        return holder;

    }

    @Override
    public void onBindViewHolder(History_View_Holder holder, int position) {

        //Use the provided View Holder on the onCreateViewHolder method to populate the current row on the RecyclerView
        holder.phoneNumber.setText(list.get(position).callNumber);
        holder.date.setText(list.get(position).callDate);

    }

    @Override
    public int getItemCount() {
        //returns the number of elements the RecyclerView will display
        return list.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    // Insert a new item to the RecyclerView on a predefined position
    public void insert(int position, CallEntry data) {
        list.add(position, data);
        notifyItemInserted(position);
    }

    // Remove a RecyclerView item containing a specified Data object
    public void remove(BlackList data) {
        int position = list.indexOf(data);
        list.remove(position);
        notifyItemRemoved(position);
    }

}

class History_View_Holder extends RecyclerView.ViewHolder {

    CardView cv;
    TextView phoneNumber;
    TextView date;

    History_View_Holder(View itemView) {
        super(itemView);
        cv = (CardView) itemView.findViewById(R.id.cardView);
        phoneNumber = (TextView) itemView.findViewById(R.id.phoneNumber);
        date = (TextView) itemView.findViewById(R.id.date);

    }
}

