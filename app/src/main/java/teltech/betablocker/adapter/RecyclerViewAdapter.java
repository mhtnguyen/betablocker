package teltech.betablocker.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import teltech.betablocker.R;
import teltech.betablocker.model.BlackList;

/**
 * Created by minhhung on 1/23/2018.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<View_Holder> {

    List<BlackList> list = Collections.emptyList();
    Context context;

    public RecyclerViewAdapter(List<BlackList> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public View_Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        //Inflate the layout, initialize the View Holder
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_layout, parent, false);
        View_Holder holder = new View_Holder(v);
        return holder;

    }

    @Override
    public void onBindViewHolder(View_Holder holder, int position) {

        //Use the provided View Holder on the onCreateViewHolder method to populate the current row on the RecyclerView
        holder.phoneNumber.setText(list.get(position).phoneNumber);
        holder.description.setText(list.get(position).description);
        //animate(holder);

    }

    @Override
    public int getItemCount() {
        //returns the number of elements the RecyclerView will display
        return list.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    // Insert a new item to the RecyclerView on a predefined position
    public void insert(int position, BlackList data) {
        list.add(position, data);
        notifyItemInserted(position);
    }

    // Remove a RecyclerView item containing a specified Data object
    public void remove(int position) {
        list.remove(position);
        notifyItemRemoved(position);
    }

    public void restoreItem(BlackList item, int position) {
        list.add(position, item);
        // notify item added by position
        notifyItemInserted(position);
    }

}

class View_Holder extends RecyclerView.ViewHolder {

    CardView cv;
    TextView phoneNumber;
    TextView description;

    View_Holder(View itemView) {
        super(itemView);
        cv = (CardView) itemView.findViewById(R.id.cardView);
        phoneNumber = (TextView) itemView.findViewById(R.id.phoneNumber);
        description = (TextView) itemView.findViewById(R.id.description);

    }
}
