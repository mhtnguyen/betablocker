package teltech.betablocker.model;

/**
 * Created by minhhung on 1/26/2018.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SpamRisk {

    @SerializedName("level")
    @Expose
    private Integer level;

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

}