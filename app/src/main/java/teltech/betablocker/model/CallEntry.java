package teltech.betablocker.model;

/**
 * Created by minhhung on 1/25/2018.
 */

public class CallEntry {

    public String callNumber;
    public String callDate;

    public CallEntry(String callNumber, String callDate) {
        this.callNumber = callNumber;
        this.callDate = callDate;
    }
}
