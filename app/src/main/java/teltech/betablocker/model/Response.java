package teltech.betablocker.model;

/**
 * Created by minhhung on 1/26/2018.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Response {

    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("recordFound")
    @Expose
    private Boolean recordFound;
    @SerializedName("phoneNumber")
    @Expose
    private String phoneNumber;
    @SerializedName("spamRisk")
    @Expose
    private SpamRisk spamRisk;
    @SerializedName("timestamp")
    @Expose
    private String timestamp;

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public Boolean getRecordFound() {
        return recordFound;
    }

    public void setRecordFound(Boolean recordFound) {
        this.recordFound = recordFound;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public SpamRisk getSpamRisk() {
        return spamRisk;
    }

    public void setSpamRisk(SpamRisk spamRisk) {
        this.spamRisk = spamRisk;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

}
