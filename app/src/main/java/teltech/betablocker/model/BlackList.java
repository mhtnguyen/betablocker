package teltech.betablocker.model;

/**
 * Created by minhhung on 1/23/2018.
 */

// Model class for database table blacklist
public class BlackList {

    // Two mapping fields for the database table blacklist
    public long id;
    public String phoneNumber;
    public String description;

    // Default constructor
    public BlackList() {

    }

    // To easily create Blacklist object, an alternative constructor
    public BlackList(final String phoneNumber, final String description ) {
        this.phoneNumber = phoneNumber;
        this.description = description;
    }

    // Overriding the default method to compare between the two objects bu phone number
    @Override
    public boolean equals(final Object obj) {

        // If passed object is an instance of Blacklist, then compare the phone numbers, else return false as they are not equal
        if(obj.getClass().isInstance(new BlackList()))
        {
            // Cast the object to Blacklist
            final BlackList bl = (BlackList) obj;

            // Compare whether the phone numbers are same, if yes, it defines the objects are equal
            if(bl.phoneNumber.equalsIgnoreCase(this.phoneNumber))
                return true;
        }
        return false;
    }
}