package teltech.betablocker.database;

/**
 * Created by minhhung on 1/23/2018.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import teltech.betablocker.model.BlackList;

public class BlackListDAO {

    // SQLiteDatabase and DatabaseHelper objects  to access SQLite database
    private SQLiteDatabase database;
    private DatabaseHelper dbHelper;

    // Constructor initiates the DatabaseHelper to make sure, database creation is done
    public BlackListDAO(Context context) {
        dbHelper = new DatabaseHelper(context);
        open();
    }

    private void open() throws SQLException {

        // Opens the database connection to provide the access 
        database = dbHelper.getWritableDatabase();
    }

    public void close() {

        // Close it, once done
        dbHelper.close();
    }

    public BlackList create(final BlackList BlackList) {

        // Steps to insert data into db (instead of using raw SQL query)
        // first, Create an object of ContentValues 
        final ContentValues values = new ContentValues();

        // second, put the key-value pair into it
        values.put("phone_number", BlackList.phoneNumber);
        values.put("description", BlackList.description);

        // thirst. insert the object into the database
        final long id = database.insert(DatabaseHelper.TABLE_BLACKLIST , null, values);

        // set the primary key to object and return back
        BlackList.id = id;
        return BlackList;
    }

    public void delete(final BlackList BlackList) {

        // Way to delete a record from database
        database.delete(DatabaseHelper.TABLE_BLACKLIST, "phone_number = '" + BlackList.phoneNumber + "'", null);
    }

    public List<BlackList> getAllBlackList() {

        // Steps to fetch all records from a database table 
        // first, create the desired object
        final List<BlackList> BlackListNumbers = new ArrayList<BlackList>();

        // second, Query the database and set the result into Cursor
        final Cursor cursor = database.query(DatabaseHelper.TABLE_BLACKLIST, new String[]{"id","phone_number","description"}, null, null, null, null, null);

        // Move the Cursor pointer to the first
        cursor.moveToFirst();

        //Iterate over the cursor
        while (!cursor.isAfterLast()) {
            final BlackList number = new BlackList();

            // Fetch the desired value from the Cursor by column index
            number.id = cursor.getLong(0);
            number.phoneNumber = cursor.getString(1);
            number.description = cursor.getString(2);
            // Add the object filled with appropriate data into the list
            BlackListNumbers.add(number);

            // Move the Cursor pointer to next for the next record to fetch
            cursor.moveToNext();
        }
        return BlackListNumbers;
    }
}
