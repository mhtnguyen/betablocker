package teltech.betablocker.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import teltech.betablocker.R;
import teltech.betablocker.fragment.BlackListFragment;
import teltech.betablocker.fragment.HistoryFragment;
import teltech.betablocker.fragment.SmsFragment;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private NavigationView nvDrawer;
    private DrawerLayout mDrawer;
    private int MY_PERMISSIONS_REQUEST_SMS_RECEIVE = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        nvDrawer = (NavigationView) findViewById(R.id.nvView);
        // Setup drawer view
        setupDrawerContent(nvDrawer);
//        ActivityCompat.requestPermissions(this,
//                new String[]{Manifest.permission.RECEIVE_SMS},
//                MY_PERMISSIONS_REQUEST_SMS_RECEIVE);

    }

    @Override
    protected void onResume() {
        super.onResume();
        //default fragment
        FragmentTransaction tx = getSupportFragmentManager().beginTransaction();
        tx.replace(R.id.flContent, new BlackListFragment());
        tx.commit();
//        final String myPackageName = getPackageName();
//        if (!Telephony.Sms.getDefaultSmsPackage(this).equals(myPackageName)) {
//            // App is not default.
//            // Show the "not currently set as the default SMS app" interface
//
//            AlertDialog.Builder builder = new AlertDialog.Builder(this);
//            builder.setMessage("This app is not set as your default messaging app. Do you want to set it default?")
//                    .setCancelable(false)
//                    .setTitle("Alert!")
//                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
//
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//
//
//                        }
//                    })
//                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                        @TargetApi(22)
//                        public void onClick(DialogInterface dialog, int id) {
//
//                            Intent intent =
//                                    new Intent(Telephony.Sms.Intents.ACTION_CHANGE_DEFAULT);
//
//                            intent.putExtra(Telephony.Sms.Intents.EXTRA_PACKAGE_NAME,
//                                    getPackageName());
//
//                            startActivity(intent);
//
//                        }
//                    });
//            builder.show();
//
//        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        selectDrawerItem(menuItem);
                        return true;
                    }
                });
    }

    public void selectDrawerItem(MenuItem menuItem) {
        Fragment fragment = null;
        Class fragmentClass;
        switch(menuItem.getItemId()) {
            case R.id.blackList:
                fragmentClass = BlackListFragment.class;
                break;
            case R.id.history:
                fragmentClass = HistoryFragment.class;
                break;
            case R.id.sms:
                fragmentClass = SmsFragment.class;
                break;
            default:
                fragmentClass = BlackListFragment.class;
        }

        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Insert the fragment, replace any existing fragment
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();

        // Highlight the selected item has been done by NavigationView
        menuItem.setChecked(true);
        // Set action bar title
        setTitle(menuItem.getTitle());
        // Close the navigation drawer
        mDrawer.closeDrawers();
    }
}
