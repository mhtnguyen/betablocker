package teltech.betablocker.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;

import teltech.betablocker.R;
import teltech.betablocker.database.BlackListDAO;
import teltech.betablocker.model.BlackList;

public class AddToBlockList extends Activity implements OnClickListener {

    // Declaration all on screen components
    private EditText country_code_et, phone_et, description_et;
    private Button reset_btn, submit_btn;
    private PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
    // Declaration of BlacklistDAO to interact with SQlite database
    private BlackListDAO blackListDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_to_block_list);

        // Initialization of the DAO object.
        blackListDao = new BlackListDAO(this);

        country_code_et = (EditText) findViewById(R.id.country_code_et);
        //only us numbers for now
        country_code_et.setFocusable(false);
        country_code_et.setFocusableInTouchMode(false);

        phone_et = (EditText) findViewById(R.id.phone_et);
        description_et = (EditText) findViewById(R.id.description_et);
        reset_btn = (Button) findViewById(R.id.reset_btn);
        submit_btn = (Button) findViewById(R.id.submit_btn);

        reset_btn.setOnClickListener(this);
        submit_btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        if (v == submit_btn) {

            // All input fields are mandatory, so made a check
            if (phone_et.getText().toString().trim().length() > 0 &&
                    description_et.getText().toString().trim().length() > 0) {

                if(!isPhoneNumberValid(phone_et.getText().toString(),"US")){
                    phone_et.setError("please enter a valid 10 digit number");
                    return;
                }
                final BlackList phone = new BlackList();
                phone.phoneNumber = "+" + country_code_et.getText().toString().replaceAll("[^0-9]", "") + phone_et.getText().toString().replaceAll("[^0-9]", "");
                ;
                phone.description = description_et.getText().toString();
                blackListDao.create(phone);
                showDialog();
            } else {
                showMessageDialog("All fields are mandatory !!");
            }
        } else if (v == reset_btn) {
            reset();
        }
    }

    public boolean isPhoneNumberValid(String phoneNumber, String countryCode) {
        try {
            Phonenumber.PhoneNumber numberProto = phoneUtil.parse(phoneNumber, countryCode);
            return phoneUtil.isValidNumber(numberProto);
        } catch (NumberParseException e) {
            System.err.println("NumberParseException was thrown: " + e.toString());
        }

        return false;
    }

    private void reset() {
        phone_et.setText("");
        description_et.setText("");
    }

    private void showDialog() {
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Phone Number added to block list successfully !!");
        alertDialogBuilder.setPositiveButton("Add More",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        reset();
                    }
                });
        alertDialogBuilder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();

                    }
                });
        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void showMessageDialog(final String message) {
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage(message);
        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}
