package teltech.betablocker.api;

/**
 * Created by minhhung on 1/26/2018.
 */

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;
import teltech.betablocker.model.Response;

public interface ApiService {

    //https://dataapi.youmail.com/api/v2/9991002000?format=json&callee=9991992999&callerId=Joe%27s+Pizza
    @Headers({
            "DataApiSid: e33ef0852366470c99c9348a0da2c62b",
            "DataApiKey: 6e01fdfe5be4e676e1e04f745b538a40f2694617e91f13a5912a82a49a4322bafa4eab6fa804b455"
    })
    @GET("/api/v2/phone/{param}")
    Call<Response> getSpamRating(@Path("param") String phoneNumber, @QueryMap Map<String, String> params);
}