package teltech.betablocker.application;

import android.app.Application;
import android.content.IntentFilter;
import android.provider.Telephony;

import teltech.betablocker.api.Client;
import teltech.betablocker.receiver.IncomingSmsReceiver;

/**
 * Created by minhhung on 1/26/2018.
 */

public class App extends Application {
    private Client client;
    private static App mInstance;
    private IncomingSmsReceiver incomingSmsReceiver;
    @Override
    public void onCreate() {
        super.onCreate();

        mInstance = this;
        client = new Client(mInstance);
        IncomingSmsReceiver incomingSmsReceiver= new IncomingSmsReceiver();
        IntentFilter filter = new IntentFilter(Telephony.Sms.Intents.SMS_RECEIVED_ACTION);
        filter.setPriority(Integer.MAX_VALUE);
        registerReceiver(incomingSmsReceiver, filter);
    }

    @Override
    public void onTerminate() {
        unregisterReceiver(incomingSmsReceiver);
        super.onTerminate();
    }

    public static synchronized App getInstance() {
        return mInstance;
    }

    public Client getClientInstance() {
        return this.client;
    }

    public static Client getClient() {
        return getInstance().getClientInstance();
    }
}