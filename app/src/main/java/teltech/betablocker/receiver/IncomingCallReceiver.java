package teltech.betablocker.receiver;

/**
 * Created by minhhung on 1/23/2018.
 */

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.media.AudioManager;
import android.os.Handler;
import android.os.Looper;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Gravity;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.internal.telephony.ITelephony;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import teltech.betablocker.R;
import teltech.betablocker.api.ApiService;
import teltech.betablocker.application.App;
import teltech.betablocker.fragment.BlackListFragment;
import teltech.betablocker.model.BlackList;
import teltech.betablocker.model.Response;
import teltech.betablocker.model.SpamRisk;

// Extend the class from BroadcastReceiver to listen when there is a incoming call
public class IncomingCallReceiver extends BroadcastReceiver {
    private String number;
    private PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
    private Phonenumber.PhoneNumber phoneNumber;
    private static LinearLayout ly;
    private static WindowManager wm;
    private static CustomPhoneStateListener phoneStateListener;

    @Override
    public void onReceive(final Context context, Intent intent) {
        try {
            wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
            TelephonyManager telephonyManager = (TelephonyManager) context
                    .getSystemService(Context.TELEPHONY_SERVICE);
            phoneStateListener = new CustomPhoneStateListener(context);
            telephonyManager.listen(phoneStateListener,
                    PhoneStateListener.LISTEN_CALL_STATE);
            // If, the received action is not a type of "Phone_State", ignore it
            if (!intent.getAction().equals("android.intent.action.PHONE_STATE"))
                return;

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void showPopup(Context context) {

        WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.TYPE_SYSTEM_ALERT |
                WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY,
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL |
                        WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSPARENT);

        params.height = WindowManager.LayoutParams.MATCH_PARENT;
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.format = PixelFormat.TRANSLUCENT;

        params.gravity = Gravity.TOP;

        ly = new LinearLayout(context);
        ly.setBackgroundColor(Color.RED);
        ly.setOrientation(LinearLayout.VERTICAL);

        wm.addView(ly, params);

    }

    // Method to disconnect phone automatically and programmatically
    @SuppressWarnings({"rawtypes", "unchecked"})
    private void disconnectPhoneItelephony(Context context) {
        ITelephony telephonyService;
        TelephonyManager telephony = (TelephonyManager)
                context.getSystemService(Context.TELEPHONY_SERVICE);
        try {
            Class c = Class.forName(telephony.getClass().getName());
            Method m = c.getDeclaredMethod("getITelephony");
            m.setAccessible(true);
            telephonyService = (ITelephony) m.invoke(telephony);
            telephonyService.endCall();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void makeSpamApiRequest(final Context context) {
        ApiService api = App.getClient().getApiService();
        Map<String, String> params = new HashMap<String, String>();
        //params.put("state", "open");

        Call<Response> call = api.getSpamRating(number, params);

        call.enqueue(new Callback<Response>() {

            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                if (response.isSuccessful()) {
                    boolean recordFound = response.body().getRecordFound();
                    if (recordFound) {
                        SpamRisk spamRisk = response.body().getSpamRisk();
                        if (spamRisk != null && spamRisk.getLevel() > 1) {
                            disconnectPhoneItelephony(context);
                            System.out.print("Spam risk score: " + spamRisk.getLevel());
                            return;
                        }
                    } else {
                        System.out.print("record not found: ");
                    }

                } else {
                    new Handler(Looper.getMainLooper()).post(new Runnable() { // Tried new Handler(Looper.myLopper()) also
                        @Override
                        public void run() {
                            Toast.makeText(context, R.string.string_some_thing_wrong, Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {

            }
        });
    }

    public class CustomPhoneStateListener extends PhoneStateListener {

        // private static final String TAG = "PhoneStateChanged";
        Context context; // Context to make Toast if required
        private AudioManager amanager;
        Intent i1;

        public CustomPhoneStateListener(Context context) {
            super();
            this.context = context;
        }

        @Override
        public void onCallStateChanged(int state, String incomingNumber) {
            super.onCallStateChanged(state, incomingNumber);

            switch (state) {
                case TelephonyManager.CALL_STATE_IDLE:
                    Toast.makeText(context, "Phone state Idle", Toast.LENGTH_LONG)
                            .show();
                    if (ly != null) {
                        wm.removeView(ly);
                    }
                    break;
                case TelephonyManager.CALL_STATE_OFFHOOK:

                    Toast.makeText(context, "Phone state Off hook", Toast.LENGTH_LONG)
                            .show();

                    if (ly != null) {
                        wm.removeView(ly);
                    }
                    break;
                case TelephonyManager.CALL_STATE_RINGING:
                    try {
                        // Fetch the number of incoming call
                        phoneNumber = phoneUtil.parse(incomingNumber, "US");
                        //make sure it has +1
                        number = phoneUtil.format(phoneNumber, PhoneNumberUtil.PhoneNumberFormat.E164);
                        //number.replaceAll("[- ]+","");
                        Log.i(IncomingCallReceiver.class.getName(), "incoming number: " + number);
                        //potential spammer just warn in red
                        if (number.contains("+4259501212")) {
                            showPopup(context);
                        }
                        // Check, whether this is a member of "Black listed" phone numbers stored in the database
                        else if (BlackListFragment.blockList.contains(new BlackList(number, ""))) {
                            // If yes, invoke the method
                            disconnectPhoneItelephony(context);
                            return;
                        } else {
                            makeSpamApiRequest(context);
                        }
                    } catch (Exception e) {
                        e.getLocalizedMessage();
                    }

                default:
                    break;
            }
        }
    }
}